package nl.utwente.di.bookQuote;

public class Quoter {
    public Quoter() {

    }

    public double getBookPrice(String isbn){
        double result = Double.parseDouble(isbn);
        return  (result*9)/5+32;
    }
}